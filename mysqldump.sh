#!/usr/bin/bash
VERSION='-- mysqldump.sh Version: 6.6 2023/11/13 http://fs4y.com/mysqldump.sh'
#Version: 6.51 2023/08/21
#Version: 6.5 2023/06/13
#Version: 6.4a 2023/03/01
ZZZ_ENDOFDATA="on"
#Version: 6.4 2023/03/01
#Version: 6.3 2023/02/04
#Version: 6.2b 2023/01/26
#Version: 6.2a 2023/01/25
#Version: 6.2 2023/01/19
#Version: 6.11a 2023/01/12
WORNINGMARK_='&#128512;'
#Version: 6.11	2023/01/08
#Version: 6.1	2023/01/07
CHECKAFILEOFSQLTABLE="on"
#Version: 6.0	2023/01/06
#Version: 5.1	2023/01/05
#Version: 5.0	2022/10/27
#Version: 4.4a	2020/12/18
# for Version: 4.4a	https://prograshi.com/platform/database/mysqldump-process-privilege/
#	ADDOPTION5732="--no-tablespaces"
#	--no-tablespaces, -y"
#Version: 4.4	2020/07/06
#Version: 4.3	2020/03/13
echo $VERSION
#2019/12/16	GNU bash : 4.2.46	- 4.4.20 (2022/11/11)
#2020/12/18	MySQL : 5.7.32		- MariaDB 10.6.7-MariaDB (2022/11/11)
#2020/12/18	mysqldump : 10.13 Distrib 5.7.32, for Linux (x86_64)
#2022/11/11	mysqldump : 10.19 Distrib 10.6.7-MariaDB, for Linux (x86_64)
#2019/12/16	phpMyAdmin : 4.8.0	- 5.1.1 (2022/11/11)

DEBUG="on"

AUTOREMOVEASSAMEMD5="on"

NumberOfSQLcharactersPerLine=1500
#	SEDSEDAWKDEBUG=debug

cd

LOGDIR='./'
if [ -d public_html/log ]
then	if [ ! -d public_html/log/mysqldump ]
	then	mkdir public_html/log/mysqldump
		if [ ! -d public_html/log/mysqldump ]
		then	echo "ERROR mkdir public_html/log/mysqldump `date`" >> public_html/log/ERROR_MYSQLDUMP.log
		else	LOGDIR='public_html/log/mysqldump/'
		fi
	fi
else	if [ -d log ]
	then	if [ ! -d log/mysqldump ]
		then	mkdir log/mysqldump
			if [ ! -d log/mysqldump ]
			then	echo "ERROR mkdir log/mysqldump `date`" >> log/ERROR_MYSQLDUMP.log
			fi
		else	LOGDIR='log/mysqldump/'
		fi
	fi
fi

export ERRORLOG="${LOGDIR}ERRORLOG.txt"

export DUMPDIR='MYSQLBACKUPS' ; if [ ! -d "${DUMPDIR}" ] ; then echo "ERROR DUMPDIR	`date`" | tee -a "$ERRORLOG" ; exit ; fi

LINE=' _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/'

function WHERE(){
 if [ ! -x "$1" ]; then
	PROCESS=`basename $0`
	echo "${LINE}
`date`
	ERROR! CHECK A FILE ---> $1
	Please modify this script $PROCESS for $1
" | tee -a "${ERRORLOG}"
	exit
 fi
}

NICE='/usr/bin/nice'	; WHERE $NICE ;		NICE="$NICE -20"

AWK='/usr/bin/awk'	; WHERE $AWK ;		AWK="$NICE $AWK"
BZIP2='/usr/bin/bzip2'  ; WHERE $BZIP2 ;	BZIP2="$BZIP2 -9" ; BZIP2="$NICE $BZIP2"
CHMOD='/usr/bin/chmod'	; WHERE $CHMOD ;	CHMOD="$NICE $CHMOD 600"
DATE='/usr/bin/date'	; WHERE $DATE
	YY=`$DATE "+%Y"`
	MM=`$DATE "+%m"`
	HHMM=`$DATE "+%H%M"`
yYY=`$DATE --date=yesterday "+%Y"`
yMM=`$DATE --date=yesterday "+%m"`
yDATE=`$DATE --date=yesterday "+%Y%m%d"`
DATE=`$DATE "+%Y%m%d"`
DIFF='/usr/bin/diff'	; WHERE $DIFF ;		DIFF="$NICE $DIFF"
FGREP='/usr/bin/fgrep'	; WHERE $FGREP ;	FGREP="$NICE $FGREP"
GREP='/usr/bin/grep'	; WHERE $GREP ;		GREP="$NICE $GREP"
MD5SUM='/usr/bin/md5sum' ; WHERE $MD5SUM ;	MD5SUM="$NICE $MD5SUM"
XAMPPBASE='/cygdrive/d/xampp7.4.10/'
if [ -x "${XAMPPBASE}mysql/bin/mysqldump.exe" ]
then	MYSQLDUMP="${XAMPPBASE}mysql/bin/mysqldump.exe"
else
	MYSQLDUMP='/usr/local/mysql/bin/mysqldump'
#	MYSQLDUMP='/usr/bin/mysqldump'
fi
	WHERE $MYSQLDUMP ;	MYSQLDUMP="$NICE $MYSQLDUMP"
if [ -x "${XAMPPBASE}mysql/bin/mysql.exe" ]
then	MYSQL="${XAMPPBASE}mysql/bin/mysql.exe"
else	MYSQL='/usr/local/mysql/bin/mysql'
fi
WHERE $MYSQL	;	MYSQL="$NICE $MYSQL"
#MYSQLHOTCOPY='/usr/local/mysql/bin/mysqlhotcopy' ; WHERE $MYSQLHOTCOPY
#	MYSQLHOTCOPY="$NICE $MYSQLHOTCOPY"
#SCP='/usr/bin/scp'	; WHERE $SCP
SED='/usr/bin/sed'	; WHERE $SED ;		SED="$NICE $SED"
SH='/usr/bin/bash'	; WHERE $SH ;		SH="$NICE $SH"
TOUCH='/usr/bin/touch'	; WHERE $TOUCH ;	TOUCH="$NICE $TOUCH -t ${DATE}${HHMM}"
#XARGS='/usr/bin/xargs'	; WHERE $XARGS ;#	XARGS="$NICE $XARGS"
ZIP='/usr/bin/zip'	; WHERE $ZIP ;		ZIP="$NICE $ZIP -9 -D -j"
#ZIP='/usr/bin/gzip'	; WHERE $ZIP ;		ZIP="$NICE $ZIP -9"
ADDZIP=".zip"

####################
if [ ! -s "${DUMPDIR}/mysqldump.tbl" ] ; then
	echo "#!/usr/bin/bash
# $VERSION

	exit ; # Finally you have to delete this line!

#
# 1.	DBNAME : Your Mysql Databasename and Username for MySQL.

#DBNAMES='Database1 Database2 Database3' ; # You have to edit para DBNAMES !!
export DBNAMES='Database1 Database2 Database3'

#
# 2.	function CASEDBNAME() has called at your each server for own process.
function CASEDBNAME(){
	DBUSER='' ; IGNORETABLES='' ; NOTIGNORETABLES=''
	case \"\$1\" in
# After this you can edit!
	Database1)	export	PASSWORD='PassWord1' ;;
#			export	DBUSER='dbLoginUserName1'
	Database2)	export	PASSWORD='PassWord2' ;;
#			export	DBUSER='dbLoginUserName2'
	Database3)	export	PASSWORD='PassWord3' ;;
#			export	DBUSER='dbLoginUserName3'
	YourID_4)	export	PASSWORD='PassWord4' ;;
#			export	DBUSER='dbLoginUserName4'
	YourID_wordpress) export PASSWORD='PassWord5'
			export	IGNORETABLES='YourID_wordpress.wp9_options'
			export	NOTIGNORETABLES='wp9_options'
#			export	DBUSER='dbLoginUserName5'

	;;
	YourID_wp)	export	PASSWORD='PassWord6'
			export	IGNORETABLES='YourID_wp.wp19_options'
			export	NOTIGNORETABLES='wp19_options'
#			export	DBUSER='dbLoginUserName6'
	;;
# Before this you can edit!
	* )	if [ \"\${ERRORLOG}\" = \"\" ] ; then ERRORLOG='./ERROR.log' ; fi
		echo \"ERROR 999	\`date\`\" >> \"\${ERRORLOG}\"
		exit
	;;
	esac
}
" > "${DUMPDIR}/mysqldump.tbl"
	$CHMOD "${DUMPDIR}/mysqldump.tbl"
	exit
fi
####################

function MAKEMD5(){
	if [ ! -f "$1" ] ; then echo "ERROR" >> "$2" ; fi
	JUNK2022=`$MD5SUM "$1"`
	JUNK2022_=`echo $JUNK2022 | sed 's/ .*//'`
	echo -n "${JUNK2022_} "  >> "$2"
	echo $JUNK2022 | sed 's%.*/%%' >> "$2"
}
function MAKEZIP(){
	if [ -s "${1}" ]
	then	$TOUCH "${1}"
		$ZIP "${1}${ADDZIP}" "${1}"
		$TOUCH "${1}${ADDZIP}"
		if [ $? -eq 0 ]
		then	rm -f "${1}"
		fi
	fi
}
function COMPRESS2(){
	if [ "${1}" != "" ]
	then	if [ -f "${1}.bz2" ] ; then  rm -f "${1}.bz2" ; fi
		if [ -f "${1}${ADDZIP}" ] ; then  rm -f "${1}${ADDZIP}" ; fi
		if [ -s "$1" ] ; then
#			$TOUCH "${1}"
#			$BZIP2 "${1}"
#			$TOUCH "${1}.bz2"
#			$CHMOD "${1}.bz2"
			MAKEZIP "${1}"
			$CHMOD "${1}${ADDZIP}"
		fi
	fi
}
function COMPMD5(){
 if [ "${1}" != "" -a "${2}" != "" ]
 then	if [ -s "${1}.MD5" -a -s "${2}.MD5" ]
	then	MD5A=`$AWK '{printf("%s",$1)}' "${1}.MD5"`
		MD5B=`$AWK '{printf("%s",$1)}' "${2}.MD5"`
		if [ "$MD5A" = "$MD5B" ]
		then	if [ -f "${2}.bz2" ] ; then rm -f "${2}.bz2" ; fi
			if [ -f "${2}${ADDZIP}" ] ; then rm -f "${2}${ADDZIP}" ; fi
			rm -f "${2}.MD5"
			if [ -f "${2}.zip.md5" ] ; then rm -f "${2}.zip.md5" ; fi
		fi
	fi
 fi
}

if [ -s "${DUMPDIR}/mysqldump.tbl" ]
then	$CHMOD "${DUMPDIR}/mysqldump.tbl"
	source "${DUMPDIR}/mysqldump.tbl"
	if [ $? -ne 0 ]
	then	echo "ERROR 001	`date`" | tee -a "${ERRORLOG}"
		exit
	fi
	if [ "$DEBUG" = "on" ] ; then
		echo "DEBUG MODE ..... START"
		echo "DBNAMES=$DBNAMES"
		for DBNAME in $DBNAMES
		do	CASEDBNAME "$DBNAME"
			echo "-----	DBNAMES=[${DBNAME}]"
			if [ "${DBUSER}" == "" ]
			then	echo "DBUSER=[${DBNAME}]"
			else	echo "DBUSER=[${DBUSER}]"
			fi
			echo "PASSWORD=[${PASSWORD}]
IGNORETABLES=[${IGNORETABLES}]
NOTIGNORETABLES=[${NOTIGNORETABLES}]
"
		done
		echo "DEBUG MODE ..... FINISHED"
		exit
	fi
else	echo "ERROR 002	`date`" | tee -a "${ERRORLOG}"
	exit
fi

$MYSQLDUMP --help > "${DUMPDIR}/mysqldump_help.$$"
if [ ! -f "${DUMPDIR}/mysqldump_help.txt" ]
then	mv "${DUMPDIR}/mysqldump_help.$$" "${DUMPDIR}/mysqldump_help.txt"
else	if [ `$DIFF "${DUMPDIR}/mysqldump_help.$$" "${DUMPDIR}/mysqldump_help.txt" | wc -l` -gt 0 ]
	then	mv "${DUMPDIR}/mysqldump_help.txt" "${DUMPDIR}/mysqldump_help${DATE}.txt"
		$BZIP2 "${DUMPDIR}/mysqldump_help${DATE}.txt"
		mv "${DUMPDIR}/mysqldump_help.$$" "${DUMPDIR}/mysqldump_help.txt"
		echo "It will be announced. \"mysqldump\" has been updated!	`date`"
		echo "$LINE"
		cat "${DUMPDIR}/mysqldump_help.txt"
		echo "$LINE"
	else	rm -f "${DUMPDIR}/mysqldump_help.$$"
	fi
fi
#	mysqldump.sh Version: 6.6
$MYSQL --help > "${DUMPDIR}/mysql_help.$$"
if [ ! -f "${DUMPDIR}/mysql_help.txt" ]
then	mv "${DUMPDIR}/mysql_help.$$" "${DUMPDIR}/mysql_help.txt"
else	if [ `$DIFF "${DUMPDIR}/mysql_help.$$" "${DUMPDIR}/mysql_help.txt" | wc -l` -gt 0 ]
	then	mv "${DUMPDIR}/mysql_help.txt" "${DUMPDIR}/mysql_help${DATE}.txt"
		$BZIP2 "${DUMPDIR}/mysql_help${DATE}.txt"
		mv "${DUMPDIR}/mysql_help.$$" "${DUMPDIR}/mysql_help.txt"
		echo "It will be announced. \"mysql\" has been updated!	`date`"
		echo "$LINE"
		cat "${DUMPDIR}/mysql_help.txt"
		echo "$LINE"
	else	rm -f "${DUMPDIR}/mysql_help.$$"
	fi
fi

function FGREPSED(){
 if [ ! -s "$1" ] ; then return ; fi
 if [ "$ZZZ_ENDOFDATA" = "on" ]
 then	cat "$1" | $SED 's/\r//g' | $SED '/^--.*/d' | $SED 's/Dump completed on.*//g' | $SED 's/AUTO_INCREMENT=[0-9]* //g' | $SED '/.* INTO .*zzz_endofdata. VALUES .*/d' | $SED '/^$/d' > "${1}$$"
 else	cat "$1" | $SED 's/\r//g' | $SED '/^--.*/d' | $SED 's/Dump completed on.*//g' | $SED 's/AUTO_INCREMENT=[0-9]* //g' | $SED '/^$/d' > "${1}$$"
 fi
 if [ -s "${1}$$" ]
 then	$MD5SUM -t "${1}$$" > "${1}.MD5"
 fi
 rm -f "${1}$$"
}

#SCP2HOSTNAME='xxxxx@s139.xrea.com'

#SQLDUMPOPTION='--single-transaction -c --order-by-primary --skip-extended-insert'
#SQLDUMPOPTION='--single-transaction --order-by-primary --skip-extended-insert'
#2023/03/03	Version: 6.4a	https://hacknote.jp/archives/27227/
SQLDUMPOPTION='--default-character-set=binary --single-transaction --order-by-primary --skip-extended-insert'

if [ "$DBNAMES" = "" ] ; then echo "ERROR 100	`date`" | tee -a "${ERRORLOG}" ; exit ; fi
for DBNAME in $DBNAMES
do	echo "<<<<<	BEGAN	$DBNAME	`date`"
	IGNORETABLES='' ; NOTIGNORETABLES='' ; TABLES='' ; MYSQL2='' ; DBUSER=''

	CASEDBNAME "$DBNAME"

	if [ "$DBUSER" = "" ] ; then DBUSER=$DBNAME ; fi
	DIRNAME2=`echo $DBNAME | $SED 'y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/'`
#	DIRNAME=`echo $DIRNAME2 | $SED 's/^.*_//'`
#	Version: 6.5	2023/06/13
	DIRNAME=`echo $DIRNAME2 | $SED 's/[^+]*_//'`
	if [ "$DIRNAME" = "$DIRNAME2" ]
	then	DIRNAME="${DIRNAME}_"
		DUMPNAME="mysql_${DIRNAME}"
	else	DUMPNAME=`echo $DBNAME | $SED 's/^.*_/mysql_/'`
	fi
#

	if [ ! -d ${DUMPDIR}/$DIRNAME ]; then
		echo "ERROR! : NOT FOUND A DIRECTORY $DIRNAME ."
		continue
	fi

	if [ -d ${DUMPDIR}/${DIRNAME}/$YY ]
	then	if [ ! -d ${DUMPDIR}/${DIRNAME}/${YY}/$MM ]
		then	mkdir ${DUMPDIR}/${DIRNAME}/${YY}/$MM
		fi
		if [ -d ${DUMPDIR}/${DIRNAME}/${YY}/$MM ]
		then	PARAMFILE=${DUMPDIR}/${DIRNAME}/${YY}/${MM}/MySQLpara.tbl
			DUMPNAME1=${DUMPDIR}/${DIRNAME}/${YY}/${MM}/${DUMPNAME}_${DATE}.sql
			DUMPNAME2=${DUMPDIR}/${DIRNAME}/${YY}/${MM}/${DUMPNAME}_${DATE}_2.sql
			DUMPNAME3=${DUMPDIR}/${DIRNAME}/${YY}/${MM}/${DUMPNAME}_${DATE}_3.sql
			if [ "$AUTOREMOVEASSAMEMD5" = "on" ] ; then
			DUMPNAME1y=${DUMPDIR}/${DIRNAME}/${yYY}/${yMM}/${DUMPNAME}_${yDATE}.sql
			DUMPNAME2y=${DUMPDIR}/${DIRNAME}/${yYY}/${yMM}/${DUMPNAME}_${yDATE}_2.sql
			DUMPNAME3y=${DUMPDIR}/${DIRNAME}/${yYY}/${yMM}/${DUMPNAME}_${yDATE}_3.sql
			fi
		else	PARAMFILE=${DUMPDIR}/${DIRNAME}/${YY}/MySQLpara.tbl
			DUMPNAME1=${DUMPDIR}/${DIRNAME}/${YY}/${DUMPNAME}_${DATE}.sql
			DUMPNAME2=${DUMPDIR}/${DIRNAME}/${YY}/${DUMPNAME}_${DATE}_2.sql
			DUMPNAME3=${DUMPDIR}/${DIRNAME}/${YY}/${DUMPNAME}_${DATE}_3.sql
			if [ "$AUTOREMOVEASSAMEMD5" = "on" ] ; then
			DUMPNAME1y=${DUMPDIR}/${DIRNAME}/${yYY}/${DUMPNAME}_${yDATE}.sql
			DUMPNAME2y=${DUMPDIR}/${DIRNAME}/${yYY}/${DUMPNAME}_${yDATE}_2.sql
			DUMPNAME3y=${DUMPDIR}/${DIRNAME}/${yYY}/${DUMPNAME}_${yDATE}_3.sql
			fi
		fi
	else	PARAMFILE=${DUMPDIR}/${DIRNAME}/MySQLpara.tbl
		DUMPNAME1=${DUMPDIR}/${DIRNAME}/${DUMPNAME}_${DATE}.sql
		DUMPNAME2=${DUMPDIR}/${DIRNAME}/${DUMPNAME}_${DATE}_2.sql
		DUMPNAME3=${DUMPDIR}/${DIRNAME}/${DUMPNAME}_${DATE}_3.sql
		if [ "$AUTOREMOVEASSAMEMD5" = "on" ] ; then
		DUMPNAME1y=${DUMPDIR}/${DIRNAME}/${DUMPNAME}_${yDATE}.sql
		DUMPNAME2y=${DUMPDIR}/${DIRNAME}/${DUMPNAME}_${yDATE}_2.sql
		DUMPNAME3y=${DUMPDIR}/${DIRNAME}/${DUMPNAME}_${yDATE}_3.sql
		fi
	fi

DBNAME=`echo $DBNAME | sed 's/+[^+]*+//' | sed 's/+//g'`

#echo \$DBNAME	:	$DBNAME
#sleep 10

if [ -f "$PARAMFILE" ]
then	\rm -f "$PARAMFILE"
	if [ $? -ne 0 ] ; then echo "ERROR04	$PARAMFILE	`date`" | tee -a "${ERRORLOG}" ; exit ; fi
fi
echo -n "" > "$PARAMFILE"
if [ $? -ne 0 ] ; then echo "ERROR05	$PARAMFILE	`date`" | tee -a "${ERRORLOG}" ; exit ; fi
$CHMOD "$PARAMFILE"
if [ $? -ne 0 ] ; then echo "ERROR06	$PARAMFILE	`date`" | tee -a "${ERRORLOG}" ; exit ; fi

echo "[mysqldump]
user=$DBUSER
password=$PASSWORD
"	>	"$PARAMFILE"
	if [ $? -ne 0 ] ; then echo "ERROR01	$PARAMFILE	`date`" | tee -a "${ERRORLOG}" ; exit ; fi
#	$CHMOD "$PARAMFILE"
#	if [ $? -ne 0 ] ; then echo "ERROR02	$PARAMFILE	`date`" | tee -a "${ERRORLOG}" ; exit ; fi

	if [ ! -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" ] # Version: 5.1 2023/01/05
	then	>	"${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl"
		if [ $? -ne 0 ] ; then echo "ERROR ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" ; exit ; fi
		echo -n "00 MAKING A FILE ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl ... "
		$MYSQL -u$DBUSER -p$PASSWORD $DBNAME -N -e "show tables" | sed 's/\r//g' > "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl"
		if [ $? -ne 0 ] ; then echo "$MYSQL -u$DBUSER -p$PASSWORD $DBNAME -N -e \"show tables\"" | tee -a "${ERRORLOG}" ; exit ; fi
		echo "done."
		$CHMOD "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl"
		if [ $? -ne 0 ] ; then echo "$CHMOD ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" | tee -a "${ERRORLOG}" ; exit ; fi
	else	if [ "$CHECKAFILEOFSQLTABLE" = "on" ] ; then # Version: 6.1 2023/01/06
			>	"${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$"
			if [ $? -ne 0 ] ; then echo "ERROR ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" | tee -a "${ERRORLOG}" ; exit ; fi
			echo -n "01 MAKING A FILE ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$ ... "
			$MYSQL -u$DBUSER -p$PASSWORD $DBNAME -N -e "show tables" | sed 's/\r//g' > "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$"
			if [ $? -ne 0 ] ; then echo "$MYSQL -u$DBUSER -p$PASSWORD $DBNAME -N -e \"show tables\" $$" | tee -a "${ERRORLOG}" ; exit ; fi
			echo "done."
			echo -n "02 CHECKING MD5 OF A FILE ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl ... "
			MYSQLTABLE_1=`$MD5SUM "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" | $AWK '{printf("%s",$1)}'`
			if [ $? -ne 0 ] ; then echo "$MD5SUM ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" | tee -a "${ERRORLOG}" ; exit ; fi
			echo "done."
			echo -n "03 CHECKING MD5 OF A FILE ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$ ... "
			MYSQLTABLE_2=`$MD5SUM "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" | $AWK '{printf("%s",$1)}'`
			if [ $? -ne 0 ] ; then echo "$MD5SUM ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" | tee -a "${ERRORLOG}" ; exit ; fi
			echo "done."
			echo "	$MYSQLTABLE_1 / $MYSQLTABLE_2"
			if [ "$MYSQLTABLE_1" != "$MYSQLTABLE_2" ]
			then	echo -n "XX	"
				echo -n "${WORNINGMARK_} !!!!! The table report is different!!!!! ${WORNINGMARK_}" | tee -a "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_DIFF.TXT"
				LANG=C ; echo "	$$	`date`" >> "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_DIFF.TXT"
				echo -n "  (PAUSING 10s)" ; sleep 10
				echo ""
				if [ ! -d "${DUMPDIR}/${DIRNAME}/_OLDdata" ]
				then	mkdir "${DUMPDIR}/${DIRNAME}/_OLDdata"
					if [ $? -ne 0 ] ; then echo "mkdir ${DUMPDIR}/${DIRNAME}/_OLDdata" | tee -a "${ERRORLOG}" ; exit ; fi
				fi
				if [ ! -f "${DUMPDIR}/${DIRNAME}/_OLDdata/MYSQLTABLE_.tbl_$$.TXT" ]
				then	echo "-- START DIFF ....." # Version: 6.11 2023/01/08
					$DIFF "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" | tee "${DUMPDIR}/${DIRNAME}/_OLDdata/MYSQLTABLE_.tbl_$$.TXT"
					if [ $? -ne 0 ] ; then echo "$DIFF ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" | tee -a "${ERRORLOG}" ; exit ; fi
					echo " done" # ; sleep 10
					echo "	-----" >> "${DUMPDIR}/${DIRNAME}/_OLDdata/MYSQLTABLE_.tbl_$$.TXT"
					cat "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" >> "${DUMPDIR}/${DIRNAME}/_OLDdata/MYSQLTABLE_.tbl_$$.TXT"
				else	echo "== START DIFF ....." # Version: 6.11 2023/01/08
					$DIFF "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" | tee "${DUMPDIR}/${DIRNAME}/_OLDdata/MYSQLTABLE_.tbl_$$$$.TXT"
					if [ $? -ne 0 ] ; then echo "$DIFF ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$$$" | tee -a "${ERRORLOG}" ; exit ; fi
					echo " done" # ; sleep 10
					echo "	-----" >> "${DUMPDIR}/${DIRNAME}/_OLDdata/MYSQLTABLE_.tbl_$$$$.TXT"
					cat "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" >> "${DUMPDIR}/${DIRNAME}/_OLDdata/MYSQLTABLE_.tbl_$$$$.TXT"
				fi
				rm -f "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl"
				if [ $? -ne 0 ] ; then echo "rm -f ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" | tee -a "${ERRORLOG}" ; exit ; fi
				mv "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl"
				if [ $? -ne 0 ] ; then echo "mv ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$ ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" | tee -a "${ERRORLOG}" ; exit ; fi
				$CHMOD "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl"
				if [ $? -ne 0 ] ; then echo "$CHMOD ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" | tee -a "${ERRORLOG}" ; exit ; fi
			else	rm -f "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$"
				if [ $? -ne 0 ] ; then echo "rm -f ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" | tee -a "${ERRORLOG}" ; exit ; fi
			fi
		fi # Version: 6.1 2023/01/06
	fi # Version: 5.1 2023/01/05

	if [ -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE.tbl" ] # Version: 6.0 2023/01/06
	then	echo "  >>>>> FOUNDED PATAMETER FILE ${DUMPDIR}/${DIRNAME}/MYSQLTABLE.tbl !"
	fi

	if [ "$IGNORETABLES" = "" ]
	then	echo $VERSION > "$DUMPNAME1"
		if [ -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE.tbl" ]
		then	$MYSQLDUMP \
			--defaults-file="$PARAMFILE" \
			--host=localhost $ADDOPTION5732 \
			$DBNAME \
			`cat "${DUMPDIR}/${DIRNAME}/MYSQLTABLE.tbl" | sed 's/\r//g'` \
			$SQLDUMPOPTION \
			>> "$DUMPNAME1"
		else	$MYSQLDUMP \
			--defaults-file="$PARAMFILE" \
			--host=localhost $ADDOPTION5732 \
			$DBNAME \
			$SQLDUMPOPTION \
			>> "$DUMPNAME1"
		fi
		FGREPSED "$DUMPNAME1"
	else	echo $VERSION > "$DUMPNAME1"
		if [ -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE.tbl" ]
		then	$MYSQLDUMP \
			--defaults-file="$PARAMFILE" \
			--host=localhost $ADDOPTION5732 \
			$DBNAME \
			`cat "${DUMPDIR}/${DIRNAME}/MYSQLTABLE.tbl" | sed 's/\r//g'` \
			$SQLDUMPOPTION \
			>> "$DUMPNAME1"
		else	$MYSQLDUMP \
			--defaults-file="$PARAMFILE" \
			--ignore-table=$IGNORETABLES \
			--host=localhost $ADDOPTION5732 \
			$DBNAME \
			$SQLDUMPOPTION \
			>> "$DUMPNAME1"
		fi
		FGREPSED "$DUMPNAME1"

		if [ -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE2.tbl" ] # Version: 6.2 2023/01/19
		then	echo "  >>>>> FOUNDED PATAMETER FILE ${DUMPDIR}/${DIRNAME}/MYSQLTABLE2.tbl !"
		fi

		echo $VERSION > "$DUMPNAME2"
		if [ -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE2.tbl" ] # Version: 6.2 2023/01/19
		then	$MYSQLDUMP \
			--defaults-file="$PARAMFILE" \
			--no-data --routines --events \
			--host=localhost \
			$SQLDUMPOPTION $ADDOPTION5732 \
			$DBNAME \
			`cat "${DUMPDIR}/${DIRNAME}/MYSQLTABLE2.tbl" | sed 's/\r//g'` \
			>> "$DUMPNAME2"
		else	$MYSQLDUMP \
			--defaults-file="$PARAMFILE" \
			--no-data --routines --events \
			--host=localhost \
			$SQLDUMPOPTION $ADDOPTION5732 \
			$DBNAME $NOTIGNORETABLES \
			>> "$DUMPNAME2"
		fi
		FGREPSED "$DUMPNAME2"

		echo $VERSION > "$DUMPNAME3"
		if [ -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE2.tbl" ] # Version: 6.2 2023/01/19
		then	$MYSQLDUMP \
			--defaults-file="$PARAMFILE" \
			--replace \
			--no-create-info \
			--host=localhost \
			$SQLDUMPOPTION $ADDOPTION5732 \
			$DBNAME \
			`cat "${DUMPDIR}/${DIRNAME}/MYSQLTABLE2.tbl" | sed 's/\r//g'` \
			>> "$DUMPNAME3"
		else	$MYSQLDUMP \
			--defaults-file="$PARAMFILE" \
			--replace \
			--no-create-info \
			--host=localhost \
			$SQLDUMPOPTION $ADDOPTION5732 \
			$DBNAME $NOTIGNORETABLES \
			>> "$DUMPNAME3"
		fi


		if [ -s "${DUMPDIR}/${DIRNAME}/SED4COMMENT.tbl" ]
		then	$SED -f "${DUMPDIR}/${DIRNAME}/SED4COMMENT.tbl" "$DUMPNAME3" > "${DUMPNAME3}$$"
			if [ -s "${DUMPNAME3}$$" -a -f "$DUMPNAME3" ]
			then	rm -f "$DUMPNAME3"
				mv "${DUMPNAME3}$$" "$DUMPNAME3"
			fi
		fi
		if [ -s "${DUMPDIR}/${DIRNAME}/SED4COMMENT2.tbl" ]
		then	$SED -r -f "${DUMPDIR}/${DIRNAME}/SED4COMMENT2.tbl" "$DUMPNAME3" > "${DUMPNAME3}$$"
			if [ -s "${DUMPNAME3}$$" -a -f "$DUMPNAME3" ]
			then	rm -f "$DUMPNAME3"
				mv "${DUMPNAME3}$$" "$DUMPNAME3"
			fi
		fi
		FGREPSED "$DUMPNAME3"
	fi
#	if [ -f "$PARAMFILE"$$ ] ; then rm -f "$PARAMFILE"$$ ; fi

#	if [ "$MYSQLHOTCOPY" != "" -a -x "$MYSQLHOTCOPY" ]; then
#		$MYSQLHOTCOPY \
#			-u $DBNAME \
#			--password=$PASSWORD \
#			$DBNAME > ${DUMPNAME}2
#	fi

	if [ "$AUTOREMOVEASSAMEMD5" = "on" ]
	then	COMPMD5 "${DUMPNAME3}" "${DUMPNAME3y}"
		COMPMD5 "${DUMPNAME2}" "${DUMPNAME2y}"
		COMPMD5 "${DUMPNAME1}" "${DUMPNAME1y}"
	fi

if [ -s "${DUMPNAME3}" ]
then	> "${DUMPNAME3}.zip.md5"
	MAKEMD5 "${DUMPNAME3}" "${DUMPNAME3}.zip.md5"
fi
if [ -s "${DUMPNAME2}" ]
then	> "${DUMPNAME2}.zip.md5"
	MAKEMD5 "${DUMPNAME2}" "${DUMPNAME2}.zip.md5"
fi
if [ -s "${DUMPNAME1}" ]
then	> "${DUMPNAME1}.zip.md5"
	MAKEMD5 "${DUMPNAME1}" "${DUMPNAME1}.zip.md5"
fi

	COMPRESS2 "${DUMPNAME3}"
	COMPRESS2 "${DUMPNAME2}"
	COMPRESS2 "${DUMPNAME1}"

if [ -s "${DUMPNAME3}.zip" ]
then	MAKEMD5 "${DUMPNAME3}.zip" "${DUMPNAME3}.zip.md5"
fi
if [ -s "${DUMPNAME2}.zip" ]
then	MAKEMD5 "${DUMPNAME2}.zip" "${DUMPNAME2}.zip.md5"
fi
if [ -s "${DUMPNAME1}.zip" ]
then	MAKEMD5 "${DUMPNAME1}.zip" "${DUMPNAME1}.zip.md5"
fi

	if [ -f "$PARAMFILE" ]
	then	rm -f "$PARAMFILE"
		if [ $? -ne 0 ] ; then echo "ERROR03	$PARAMFILE	`date`" | tee -a "${ERRORLOG}" ; exit ; fi
	fi

	echo ">>>>>	DONE	$DBNAME	`date`"
done
echo "`date`	SCRIPT $0 Done."

exit
Version: 6.6
+ $MYSQL --help > "${DUMPDIR}/mysql_help.$$"

Version: 6.51 2023/08/21
+ $CHMOD "${DUMPDIR}/mysqldump.tbl"
- if [ -f "$PARAMFILE" ] ; then \rm -f "$PARAMFILE" ; fi
+ if [ -f "$PARAMFILE" ]
  then	\rm -f "$PARAMFILE"
	if [ $? -ne 0 ] ; then echo "ERROR04	$PARAMFILE	`date`" | tee -a "${ERRORLOG}" ; exit ; fi
  fi
  echo -n "" > "$PARAMFILE"
  if [ $? -ne 0 ] ; then echo "ERROR05	$PARAMFILE	`date`" | tee -a "${ERRORLOG}" ; exit ; fi
  $CHMOD "$PARAMFILE"
  if [ $? -ne 0 ] ; then echo "ERROR06	$PARAMFILE	`date`" | tee -a "${ERRORLOG}" ; exit ; fi

Version: 6.5 2023/06/13
- DIRNAME=`echo $DIRNAME2 | $SED 's/^.*_//'`
+ DIRNAME=`echo $DIRNAME2 | $SED 's/[^+]*_//'`
+ if [ -f "$PARAMFILE" ] ; then \rm -f "$PARAMFILE" ; fi

Version: 6.4a 2023/03/01
+ ZZZ_ENDOFDATA="on"
+ if [ "$ZZZ_ENDOFDATA" = "on" ]
+ then	cat "$1" | $SED 's/\r//g' | $SED '/^--.*/d' | $SED 's/Dump completed on.*//g' | $SED 's/AUTO_INCREMENT=[0-9]* //g' | $SED '/.* INTO .*zzz_endofdata. VALUES .*/d' | $SED '/^$/d' > "${1}$$"
+ else	cat "$1" | $SED 's/\r//g' | $SED '/^--.*/d' | $SED 's/Dump completed on.*//g' | $SED 's/AUTO_INCREMENT=[0-9]* //g' | $SED '/^$/d' > "${1}$$"
+ fi
+	--default-character-set=binary

Version: 6.4 2023/03/01
+ yYY=`$DATE --date=yesterday "+%Y"`
+ yMM=`$DATE --date=yesterday "+%m"`

Version: 6.2b 2023/01/26
- cat "$1" | $SED 's/Dump completed on.*//g' | $SED 's/AUTO_INCREMENT=[0-9]* //g' > "${1}$$"
+ cat "$1" | $SED '/^--.*/d' | $SED 's/Dump completed on.*//g' | $SED 's/AUTO_INCREMENT=[0-9]* //g' > "${1}$$"

Version: 6.2a 2023/01/25
- $FGREP -v "Dump completed on" "$1" | $SED 's/AUTO_INCREMENT=[0-9]* //g' > "${1}$$"
+ cat "$1" | $SED 's/Dump completed on.*//g' | $SED 's/AUTO_INCREMENT=[0-9]* //g' > "${1}$$"

Version: 6.2 2023/01/19
+ if [ -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE2.tbl" ] # Version: 6.2 2023/01/19
  then	echo " FOUNDED PATAMETER FILE ${DUMPDIR}/${DIRNAME}/MYSQLTABLE2.tbl !"
  fi
+ if [ -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE2.tbl" ] # Version: 6.2 2023/01/19
  then	$MYSQLDUMP \
	--defaults-file="$PARAMFILE" \
	--no-data --routines --events \
	--host=localhost \
	$SQLDUMPOPTION $ADDOPTION5732 \
	$DBNAME \
	`cat "${DUMPDIR}/${DIRNAME}/MYSQLTABLE2.tbl" | sed 's/\r//g'` \
	>> "$DUMPNAME2"
  else	$MYSQLDUMP \
	--defaults-file="$PARAMFILE" \
	--no-data --routines --events \
	--host=localhost \
	$SQLDUMPOPTION $ADDOPTION5732 \
	$DBNAME $NOTIGNORETABLES \
	>> "$DUMPNAME2"
  fi
+ if [ -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE2.tbl" ] # Version: 6.2 2023/01/19
  then	$MYSQLDUMP \
	--defaults-file="$PARAMFILE" \
	--replace \
	--no-create-info \
	--host=localhost \
	$SQLDUMPOPTION $ADDOPTION5732 \
	$DBNAME \
	`cat "${DUMPDIR}/${DIRNAME}/MYSQLTABLE2.tbl" | sed 's/\r//g'` \
	>> "$DUMPNAME3"
  else	$MYSQLDUMP \
	--defaults-file="$PARAMFILE" \
	--replace \
	--no-create-info \
	--host=localhost \
	$SQLDUMPOPTION $ADDOPTION5732 \
	$DBNAME $NOTIGNORETABLES \
	>> "$DUMPNAME3"
  fi

Version: 6.11a 2023/01/12
+ WORNINGMARK_='&#128512;'

Version: 6.11 2023/01/08
+ $DIFF "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" | tee "${DUMPDIR}/${DIRNAME}/_OLDdata/MYSQLTABLE_.tbl_$$.TXT"
  if [ $? -ne 0 ] ; then echo "$DIFF ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" ; exit ; fi
  echo " done" # ; sleep 10
  echo "	-----" >> "${DUMPDIR}/${DIRNAME}/_OLDdata/MYSQLTABLE_.tbl_$$.TXT"
  cat "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" >> "${DUMPDIR}/${DIRNAME}/_OLDdata/MYSQLTABLE_.tbl_$$.TXT"

Version: 6.1 2023/01/07
+ CHECKAFILEOFSQLTABLE="on"
+ if [ "$CHECKAFILEOFSQLTABLE" = "on" ] ; then # Version: 6.1 2023/01/06
	>	"${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$"
	if [ $? -ne 0 ] ; then echo "ERROR ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" ; exit ; fi
	echo -n " MAKING A FILE ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$ ... "
	$MYSQL -u$DBUSER -p$PASSWORD $DBNAME -N -e "show tables" | sed 's/\r//g' > "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$"
	if [ $? -ne 0 ] ; then echo "$MYSQL -u$DBUSER -p$PASSWORD $DBNAME -N -e \"show tables\" $$" ; exit ; fi
	echo "done."
	echo -n " CHECKING MD5 OF A FILE ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl ... "
	MYSQLTABLE_1=`$MD5SUM "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" | $AWK '{printf("%s",$1)}'`
	if [ $? -ne 0 ] ; then echo "$MD5SUM ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" ; exit ; fi
	echo "done."
	echo -n " CHECKING MD5 OF A FILE ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$ ... "
	MYSQLTABLE_2=`$MD5SUM "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" | $AWK '{printf("%s",$1)}'`
	if [ $? -ne 0 ] ; then echo "$MD5SUM ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" ; exit ; fi
	echo "done."
	echo "$MYSQLTABLE_1	/	$MYSQLTABLE_2"
	if [ "$MYSQLTABLE_1" != "$MYSQLTABLE_2" ]
	then	echo -n " !!!!! The table report is different!!!!! (PAUSING 10s)" ; sleep 10
		echo ""
		rm -f "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl"
		if [ $? -ne 0 ] ; then echo "rm -f ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" ; exit ; fi
		mv "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl"
		if [ $? -ne 0 ] ; then echo "mv ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$ ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" ; exit ; fi
		$CHMOD "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl"
		if [ $? -ne 0 ] ; then echo "$CHMOD ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" ; exit ; fi
	else	rm -f "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$"
		if [ $? -ne 0 ] ; then echo "rm -f ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl$$" ; exit ; fi
	fi
  fi # Version: 6.1 2023/01/06

Version: 6.0	2023/01/06
+ DBNAME=`echo $DBNAME | sed 's/+[^+]*+//' | sed 's/+//g'`
+ if [ -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE.tbl" ]
  then	echo " FOUNDED PATAMETER FILE ${DUMPDIR}/${DIRNAME}/MYSQLTABLE.tbl !"
  fi


Version: 5.1	2023/01/05
+ if [ -x "${XAMPPBASE}mysql/bin/mysql.exe" ]
  then	MYSQL="${XAMPPBASE}mysql/bin/mysql.exe"
  else	MYSQL='/usr/local/mysql/bin/mysql'
  fi
  WHERE $MYSQL	;	MYSQL="$NICE $MYSQL"
+ if [ ! -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" ] # Version: 5.1 2023/01/05
  then	>	"${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl"
	if [ $? -ne 0 ] ; then echo "ERROR ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" ; exit ; fi
	echo -n " MAKING A FILE ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl ... "
	$MYSQL -u$DBUSER -p$PASSWORD $DBNAME -N -e "show tables" | sed 's/\r//g' > "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl"
	if [ $? -ne 0 ] ; then echo "$MYSQL -u$DBUSER -p$PASSWORD $DBNAME -N -e \"show tables\"" ; exit ; fi
	echo "done."
	$CHMOD "${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl"
	if [ $? -ne 0 ] ; then echo "$CHMOD ${DUMPDIR}/${DIRNAME}/MYSQLTABLE_.tbl" ; exit ; fi
  fi # Version: 5.1 2023/01/05
+ if [ -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE.tbl" ]
  then	$MYSQLDUMP \
	--defaults-file="$PARAMFILE" \
	--host=localhost $ADDOPTION5732 \
	$DBNAME \
	`cat "${DUMPDIR}/${DIRNAME}/MYSQLTABLE.tbl" | sed 's/\r//g'` \
	$SQLDUMPOPTION \
	>> "$DUMPNAME1"
  else	$MYSQLDUMP \
	--defaults-file="$PARAMFILE" \
	--host=localhost $ADDOPTION5732 \
	$DBNAME \
	$SQLDUMPOPTION \
	>> "$DUMPNAME1"
  fi
+ if [ -s "${DUMPDIR}/${DIRNAME}/MYSQLTABLE.tbl" ]
  then	$MYSQLDUMP \
	--defaults-file="$PARAMFILE" \
#	--ignore-table=$IGNORETABLES \
	--host=localhost $ADDOPTION5732 \
	$DBNAME \
	`cat "${DUMPDIR}/${DIRNAME}/MYSQLTABLE.tbl" | sed 's/\r//g'` \
	$SQLDUMPOPTION \
	>> "$DUMPNAME1"
  else	$MYSQLDUMP \
	--defaults-file="$PARAMFILE" \
	--ignore-table=$IGNORETABLES \
	--host=localhost $ADDOPTION5732 \
	$DBNAME \
	$SQLDUMPOPTION \
	>> "$DUMPNAME1"
  fi

Version: 5.0	2022/10/27
+ function MAKEMD5(){
	if [ ! -f "$1" ] ; then echo "ERROR" >> "$2" ; fi
	JUNK2022=`$MD5SUM "$1"`
	JUNK2022_=`echo $JUNK2022 | sed 's/ .*//'`
	echo -n "${JUNK2022_} "  >> "$2"
	echo $JUNK2022 | sed 's%.*/%%' >> "$2"
  }
+ if [ -f "${2}.zip.md5" ] ; then rm -f "${2}.zip.md5" ; fi

Version: 4.4a 2020/12/18
#	ADDOPTION5732="--no-tablespaces"

Version: 4.4 2020/07/06
+ if [ -s "${DUMPDIR}/${DIRNAME}/SED4COMMENT2.tbl" ]
  then	$SED -r -f "${DUMPDIR}/${DIRNAME}/SED4COMMENT2.tbl" "$DUMPNAME3" > "${DUMPNAME3}$$"
	if [ -s "${DUMPNAME3}$$" -a -f "$DUMPNAME3" ]
	then	rm -f "$DUMPNAME3"
		mv "${DUMPNAME3}$$" "$DUMPNAME3"
	fi
  fi


Version: 4.3 2020/03/13
+ XAMPPBASE='/cygdrive/d/xampp7.4.3/'
+ if [ -x "${XAMPPBASE}mysql/bin/mysqldump.exe" ]
+ then	MYSQLDUMP="${XAMPPBASE}mysql/bin/mysqldump.exe"

Version: 4.2a 2020/01/05
+ if [ -x /cygdrive/d/xampp/mysql/bin/mysqldump.exe ]
+ then	MYSQLDUMP='/cygdrive/d/xampp/mysql/bin/mysqldump.exe'
+ else
+	MYSQLDUMP='/usr/local/mysql/bin/mysqldump'
+ #	MYSQLDUMP='/usr/bin/mysqldump'
+ fi

Version: 4.2 2020/01/03
+ #			export	DBUSER='dbLoginUserNameX' for mysqldump.tbl
+ if [ "$DBUSER" = "" ] ; then DBUSER=$DBNAME ; fi
- echo "[mysqldump]
- user=$DBNAME
+ echo "[mysqldump]
+ user=$DBUSER
- if [ "$DIRNAME" = "$DIRNAME2" ]
- then	DIRNAME="MAIN_"
-	DUMPNAME="mysql_MAIN_"
+ if [ "$DIRNAME" = "$DIRNAME2" ]
+ then	DIRNAME="${DIRNAME}_"
+	DUMPNAME="mysql_${DIRNAME}"


Version: 4.1 2019/12/28
+ DIRNAME2=`echo $DBNAME | $SED 'y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/'`
  DIRNAME=`echo $DIRNAME2 | $SED 's/^.*_//'`
  if [ "$DIRNAME" = "$DIRNAME2" ]
  then	DIRNAME="MAIN_"
  	DUMPNAME="mysql_MAIN_"
  else	DUMPNAME=`echo $DBNAME | $SED 's/^.*_/mysql_/'`
  fi
+ if [ -s "${DUMPDIR}/${DIRNAME}/SED4COMMENT.tbl" ]
  then	$SED -f "${DUMPDIR}/${DIRNAME}/SED4COMMENT.tbl" "$DUMPNAME3" > "${DUMPNAME3}$$"
	if [ -s "${DUMPNAME3}$$" -a -f "$DUMPNAME3" ]
	then	rm -f "$DUMPNAME3"
		mv "${DUMPNAME3}$$" "$DUMPNAME3"
	fi
  fi


Version: 4.0b 2019/12/16
- ALL OF "function SEDSEDAWK()" ERASED !  and
	MOVED THERE -->
			####################
			if [ ! -s "${DUMPDIR}/mysqldump.tbl" ] ; then
			fi
			####################
- CHMOD='/usr/bin/chmod'	; WHERE $CHMOD
+ CHMOD='/usr/bin/chmod'	; WHERE $CHMOD ;	CHMOD="$NICE $CHMOD 600"
+ HHMM=`$DATE "+%H%M"`
+ TOUCH='/usr/bin/touch'	; WHERE $TOUCH ;	TOUCH="$NICE $TOUCH -t ${DATE}${HHMM}"
+ $CHMOD "${DUMPDIR}/mysqldump.tbl"
+ $BZIP2 "${DUMPDIR}/mysqldump_help${DATE}.txt"

Version: 4.0a 2019/12/13
+ if [ -f "$PARAMFILE" ]
+ then	rm -f "$PARAMFILE"
+	if [ $? -ne 0 ] ; then echo "ERROR03	$PARAMFILE	`date`" | tee -a "${ERRORLOG}" ; exit ; fi
+ fi

Version: 4.0  2019/12/13
- -u $DBNAME
- --password=$PASSWORD
+ --defaults-file="$PARAMFILE" \

Version: 3.2 2019/12/12
+ SQLDUMPOPTION='--single-transaction --order-by-primary --skip-extended-insert'
- .bz2
+ .sql.zip

Version: 3.1 2019/12/11
  function SEDSEDAWK(){
   if [ ! -s "$1" ] ; then return ; fi
+  if [ "$2" = "debug" ]

- SEDSEDAWK "$DUMPNAMEx"
+ SEDSEDAWK "$DUMPNAMEx" $SEDSEDAWKDEBUG

- $AWK 'BEGIN{A=500;Z=A;COM2=""}{B=length($0);if($2 == "INTO" && $4 == "VALUES")COM=$0;if(index($1,"(")==1){if(COM2!=""){print COM2;COM2=""}Z = Z - B;if(Z < 0){COM2=COM;Z=A;printf("%s;\n",substr($0,1,B - 1))}else print $0}else{print $0;Z=A}}'
+ $AWK -v NOSCPL=${NumberOfSQLcharactersPerLine} -v VERSION="${VERSION}" 'BEGIN{NOSCPL=NOSCPL + 0;Z=NOSCPL;COM="";BEFOREDATA="";START=0;}{if($2 == "INTO" && $4 == "VALUES"){if(BEFOREDATA != ""){printf("%s;\n",BEFOREDATA);}COM=$0;print $0;Z=NOSCPL;BEFOREDATA="";START=1;next;}if(index($1,"(")==1){if(BEFOREDATA != ""){Z = Z - length(BEFOREDATA) - 2;if(Z < 0){print ";";print VERSION;printf("-- %06d %-06d\n",NOSCPL,Z);print COM;Z=NOSCPL;}else{if(START==0){print ",";}}printf("%s",BEFOREDATA);START=0;if(length(BEFOREDATA) > NOSCPL){print ";";print COM;Z=NOSCPL;}}BEFOREDATA=substr($0,1,length($0) - 1);}else{if(BEFOREDATA != ""){printf(",\n%s;\n",BEFOREDATA);BEFOREDATA="";}print $0;}}'


Version: 3.0a 2019/12/09
+ SQLDUMPOPTION='--single-transaction'
